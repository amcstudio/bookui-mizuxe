"use strict";
$(document).ready(function() {
  var $bodyElement = $("body");

  $bodyElement.scrollspy({ target: "#main-nav" });
  console.log("hi");
  // Add smooth scrolling
  $("#main-nav a").on("click", function(e) {
    $("#navbarCollapse").collapse("hide");
    // check for hash values
    if (this.hash !== "") {
      e.preventDefault();
      // prevent default behaviour

      // store hash
      var hash = this.hash;

      // Animate smooth   scroll
      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top
        },
        900,
        function() {
          // Add hash to url after scroll

          window.location.hash = hash;
        }
      );
    }
  });
});
